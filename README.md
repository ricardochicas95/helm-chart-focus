## Usage
Open your terminal in _helm-chart-focus/charts/test-focus_ and run:

First check if a service is running
```bash
~$ helm list
```
If not run to create the service in kubernetes cluster
```bash
~$ helm install <name> ./
```
then check again if a service is running.

Last, run this command to refresh the cluster with any change you made. 
```bash
~$ helm upgrade <name> .
```

To check if changes were applied run.
```bash
~$ kubectl get services -o wide
~$ kubectl get nodes -o wide
~$ kubectl get pods -o wide
```

To run the app in local machine run
```bash
~$ kubectl port-forward <POD NAME> 8080:80
```
